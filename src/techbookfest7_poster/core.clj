(ns techbookfest7-poster.core
  (:gen-class)
  (:require [quil.core :as q]
            [quil.middleware :as m]))

;;;; イメージ
;;
;; - 曼荼羅
;; - 7つのLispに掛けた7つの円
;; - ()と{}と[]
;; - 真ん中に鎮座するnil
;; https://twitter.com/sin_clav/status/1084138434829082624

;;;; 入れる文字
;;
;; サークルのこと: paren-holic
;; 頒布物 (新刊のこと、既刊のこと)

;; (def scale 900)  ; A2サイズの入稿用設定
(def scale 200)  ; プレビュー時の設定
(def dpi 150)
(def width (* (/ 420 dpi) scale))
(def height (* (/ 594 dpi) scale))

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb)
  {:tick 0
   :sans (q/create-font "Noto Sans CJK JP Black" 30)
   :serif (q/create-font "Noto Serif CJK JP" 30)})

(defn update-state [state]
  {:tick (+ (:tick state))
   :sans (:sans state)
   :serif (:serif state)})

(defn draw-pring [c n r s ch]
  (q/text-align :left)
  (q/text-size (* (/ scale dpi 3.3) 30 s))
  (q/push-matrix)
  (q/translate (:x c) (:y c))
  (doseq [i (range 0 n)]
    (q/rotate (q/radians (/ 360 n)))
    (q/text ch r 0))
    ;; (q/fill 0 255 255 10)
    ;; (q/rect r 0 70 70))
  (q/pop-matrix))

(defn draw-pcircle [c r a d s ch]
  (doseq [n (range 1 20 d)]
    (q/fill 255 0 255 (* (* 255 (* n n 0.004)) a))
    (draw-pring c (* 1.8 n) (+ (* 2 r) (* (/ scale dpi 3.14) r n)) (* 0.12 n s) ch)))

(defn draw-mandara [a s]
  (q/push-matrix)
  (draw-pcircle {:x (/ width 2) :y (/ height 2)}
                16 a 1 s "()")
  (q/translate (/ width 2) (/ height 2))
  (let [r (* width 0.4)
        ch "([{)]}"]
    (doseq [n (range 0 6)]
      (q/push-matrix)
      (q/translate (* r (q/cos (* n (/ q/TWO-PI 6))))
                   (* r (q/sin (* n (/ q/TWO-PI 6)))))
      (draw-pcircle {:x 0 :y 0} 10 (* 0.7 a) 2 s (str (nth ch n)))
      (q/pop-matrix))
    (doseq [n (range 0 12)]
      (q/push-matrix)
      (q/translate (* (* 1.5 r) (q/cos (* n (/ q/TWO-PI 12))))
                   (* (* 1.5 r) (q/sin (* n (/ q/TWO-PI 12)))))
      (draw-pcircle {:x 0 :y 0} 10 (* 0.7 a) 2 s (str (nth ch (mod n 6))))
      (q/pop-matrix)))
  (q/pop-matrix))

(defn draw-copy []
  (q/text-align :center)
  (q/fill 255)
  (q/text-size (* width 0.1))
  (q/text-leading (* width 0.01))
  (q/text "paren-holicの" (/ width 2) (* height 0.1))
  (q/text "括弧まみれの" (/ width 2) (* height 0.2))
  (q/text "新刊あります" (/ width 2) (* height 0.3)))

(defn draw-bookinfo []
  (q/fill 255)
  (q/text-align :left)
  (q/text-size (* width 0.05))
  (q/text-leading (* width 0.06))
  (let [info (str "『Clojureに入門したら知って\n　おきたい5つのこと』\n"
                  "『Lispとコンピュータ音楽 Vol.1』")]
    (q/text info (* width 0.1) (* height 0.75)))
  (q/text-align :right)
  (q/text-size (* width 0.04))
  (q/text-leading (* width 0.05))
  (q/text "どちらも単品500円\n紙電子セット1000円"
          (* width 0.9) (* height 0.89)))

(defn draw-state [state]
  (q/text-font (:serif state))
  (q/background 0)
  (draw-mandara 0.4 1)
  (draw-mandara 0.015 6)
;  (draw-mandara 0.01 9)
  (q/text-font (:sans state))
  (draw-copy)
  (draw-bookinfo)
  (when (= (:tick state) 0)
    (q/save-frame "mandara.png")))

(q/defsketch techbookfest7-poster
  :title "poster"
  :size [width height]
  :setup setup
  :update update-state
  :draw draw-state
  :features [:keep-on-top]
  :middleware [m/fun-mode])

(defn -main [& args])
